/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Order {

    private int id;
    private Date created;
    private int empId;
    private int cusId;
    private double total;
    private String empName;
    private String cusName;

    public Order(int id, Date created, int cusId, int empId) {
        this.id = id;
        this.created = created;
        this.empId = empId;
        this.cusId = cusId;
    }

    public Order(int id, int cusId, int empId, double total) {
        this.id = id;
        this.empId = empId;
        this.cusId = cusId;
        this.total = total;

    }

    public Order(int cusId, int empId, double change) {
        this.empId = empId;
        this.cusId = cusId;
        this.total = change;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getTotal() {
        return total;
    }

    public int getId() {
        return id;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getEmpName() {
        return empName;
    }

    public String getCusName() {
        return cusName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", created=" + created + ", empId=" + empId + ", cusId=" + cusId + ", total=" + total + ", empName=" + empName + ", cusName=" + cusName + '}';
    }
    
}
