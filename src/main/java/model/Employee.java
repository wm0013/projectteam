/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ASUS
 */
public class Employee {

    private int empId;
    private String empName;
    private String empLName;
    private String empTel;
    private Double empSalary;

    public Employee(int id, String name, String lname, String tel, double salary) {
        this.empId = id;
        this.empName = name;
        this.empLName = lname;
        this.empTel = tel;
        this.empSalary = salary;
    }

    public Employee(int empId, String empName, String empTel) {
        this.empId = empId;
        this.empName = empName;
        this.empTel = empTel;
    }

    public Employee(String text) {
      this.empId=Integer.parseInt(text);
    }

    public Employee(int empId) {
        this.empId=empId;
    }

  
   

    public int getId() {
        return empId;
    }

    public void setId(int id) {
        this.empId = id;
    }

    public String getName() {
        return empName;
    }

    public void setName(String name) {
        this.empName = name;
    }

    public String getLname() {
        return empLName;
    }

    public void setLname(String lname) {
        this.empLName = lname;
    }

    public void setTel(String tel) {
        this.empTel = tel;
    }

    public String getTel() {
        return empTel;
    }

    public void setSalary(double salary) {
        this.empSalary = salary;
    }

    public double getSalary() {
        return empSalary;
    }

    @Override
    public String toString() {
        return empId + " " + empName + " " + empLName + " " + empTel + " " + empSalary;
    }
}
