/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.User;

/**
 *
 * @author user
 */
public class UserDao implements DaoInterface<User> {

    @Override
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO user (user_name,user_lname,user_status)VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getLname());
            stmt.setString(3, object.getStatus());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: fail to add");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT user_id,user_name,user_lname,user_status,emp_id FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int user_id = result.getInt("user_id");
                String user_name = result.getString("user_name");
                String user_lname = result.getString("user_lname");
                String user_status = result.getString("user_status");
                int emp_id=result.getInt("emp_id");
                User user = new User(user_id, user_name, user_lname, user_status,emp_id);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by this :" + ex);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT user_id,user_name,user_lname,user_status FROM user WHERE user_id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int user_id = result.getInt("user_id");
                String user_name = result.getString("user_name");
                String user_lname = result.getString("user_lname");
                String user_status = result.getString("user_status");
                User user = new User(user_id, user_name, user_lname, user_status);

                return user;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: fail to get data");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM user WHERE user_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR: failt to delete");
        }

        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user SET user_name = ?,user_lname = ?,user_status = ? WHERE user_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getLname());
            stmt.setString(3, object.getStatus());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();
            System.out.println("-----------");
        } catch (SQLException ex) {
            System.out.println("ERROR: fail to update");
            System.out.println(object.toString());
        }

        db.close();
        return row;
    }

    public User getProfile(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT user_id,user_name,user_lname,user_status FROM user WHERE user_id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                 int user_id = result.getInt("user_id");
                String user_name = result.getString("user_name");
                String user_lname = result.getString("user_lname");
                String user_status = result.getString("user_status");
                User user = new User(user_id, user_name, user_lname, user_status);

                return user;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: fail to get data");
        }
        db.close();
        return null;
    }

    public ArrayList<User> getSearch(String object) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT user_id,user_name,user_lname,user_status FROM user "
                    + "WHERE user_name LIKE" + "\'" + object + "%" + "\'"
                    + "OR user_id Like" + "\'" + object + "%" + "\'"
                    + "OR user_lname Like" + "\'" + object + "%" + "\'"
                    + "OR user_status Like" + "\'" + object + "%" + "\'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int user_id = result.getInt("user_id");
                String user_name = result.getString("user_name");
                String user_lname = result.getString("user_lname");
                String user_status = result.getString("user_status");
                User user = new User(user_id, user_name, user_lname, user_status);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getName" + ex.getMessage());
        }
        db.close();
        return list;
    }

    public static void main(String[] args) {

    }

}
