/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import jdk.nashorn.internal.ir.EmptyNode;
import model.Customer;
import model.Employee;
import model.Order;
import model.OrderDetail;
import model.Product;

/**
 *
 * @author user
 */
public class OrderDao implements DaoInterface<Order> {

    @Override
    public int add(Order object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        int id = -1;
        try {
            String sql = "INSERT INTO [order] ( cus_id, emp_id,order_total,cus_Name,emp_Name )VALUES (?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCusId());
            stmt.setInt(2, object.getEmpId());
            stmt.setDouble(3, object.getTotal());
            stmt.setString(4, object.getCusName());
            stmt.setString(5, object.getEmpName());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
//            for (OrderDetail o : object.getOrderDetail()) {
//                String sqlDetaill = "INSERT INTO order_detail (order_id,product_id,price,amount) VALUES (?,?,?,?);";
//                PreparedStatement stmtDetaill = conn.prepareStatement(sqlDetaill);
//                stmtDetaill.setInt(1, o.getOrder().getId());
//                stmtDetaill.setInt(2, o.getProduct().getId());
//                stmtDetaill.setDouble(3, o.getPrice());
//                stmtDetaill.setDouble(4, o.getAmount());
//                int rowDetaill = stmtDetaill.executeUpdate();
//                ResultSet resultDetaill = stmtDetaill.getGeneratedKeys();
//                if (resultDetaill.next()) {
//                    id = resultDetaill.getInt(1);
//                    o.setId(id);
//                }
//            }
//            for (OrderDetail r : object.getOrderDetail()) {
//                String sqlDetail = "INSERT INTO order ( customer_id, emp_id,total )VALUES (?,?,?);";
//                PreparedStatement stmtDetaill = conn.prepareStatement(sql);
//                stmtDetaill.setInt(1, object.getCusId());
//                stmtDetaill.setInt(2, object.getEmpId());
//                stmtDetaill.setDouble(3, object.getTotal());
//                int rowDetail = stmtDetaill.executeUpdate();
//                ResultSet resultDetail = stmtDetaill.getGeneratedKeys();
//                if (resultDetail.next()) {
//                    id = resultDetail.getInt(1);
//                }
//            }
        } catch (SQLException ex) {
            System.out.println("Error: to create receipt" + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Order> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT order_id,cus_id,emp_id,order_total,cus_Name,emp_Name FROM [order]";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("order_id");
                int cusId = result.getInt("cus_id");
                String cusName = result.getString("cus_Name");
                int empId = result.getInt("emp_id");
                String empName = result.getString("emp_Name");
                double total = result.getDouble("order_total");
                Order order = new Order(id, cusId, empId, total);
                order.setCusName(cusName);
                order.setEmpName(empName);
                list.add(order);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all order" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Order get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT order_id,cus_id,emp_id,order_total,cus_Name,emp_Name FROM [order]";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int rid = result.getInt("order_id");
                int cusId = result.getInt("cus_id");
                String cusName = result.getString("cus_Name");
                int empId = result.getInt("emp_id");
                String empName = result.getString("emp_Name");
                double total = result.getDouble("order_total");
                Order order = new Order(id, cusId, empId, total);
                order.setCusName(cusName);
                order.setEmpName(empName);
//                getOrderDetaill(conn, id, order);
                return order;
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id" + id + ex.getMessage());
        }
        db.close();
        return null;
    }

    public ArrayList<OrderDetail> getOrderDetail(int id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
        String sqlDetail = "SELECT od.order_Id as od_id, p.product_name as product_name ,od.orderD_Amount as orderD_Amount , od.orderD_Price as orderD_Price FROM order_detail od JOIN product p WHERE order_Id = ? AND od.product_Id = p.product_id";
        PreparedStatement stmtDetaill = conn.prepareStatement(sqlDetail);
        stmtDetaill.setInt(1, id);
        ResultSet resultDetaill = stmtDetaill.executeQuery();
        while (resultDetaill.next()) {
            int orderDId = resultDetaill.getInt("od_id");
            String product_name = resultDetaill.getString("product_name");
            int orderD_Amount = resultDetaill.getInt("orderD_Amount");
            double orderD_Price = resultDetaill.getDouble("orderD_Price");
            OrderDetail o = new OrderDetail(orderDId, product_name, orderD_Amount, orderD_Price);
            list.add(o);
        }
            return list;
         } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id ++++" + id +" "+ ex.getMessage());
        }
        db.close();
        return null;
    }


    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        int row = 0;
        try {
            String sql = "DELETE FROM [order] WHERE order_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete receipt id" + id);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Order object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        int row = 0;
        try {
            String sql = "UPDATE [order] SET cus_id = ?,emp_id = ?,order_total = ?,cus_Name = ?,emp_Name = ? WHERE order_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCusId());
            stmt.setInt(2, object.getEmpId());
            stmt.setDouble(3, object.getTotal());
            stmt.setString(4, object.getCusName());
            stmt.setString(5, object.getEmpName());
            stmt.setInt(6, object.getId());

            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :" + ex);
        }
        db.close();
        return 0;
    }

    public ArrayList<Order> getSearch(String object) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT order_id,cus_id,emp_id,order_total,cus_Name,emp_Name FROM [order] WHERE order_id LIKE" + "'" + object + "%" + "'" + "OR cus_Name Like" + "'" + object + "%" + "'" + "OR emp_Name Like" + "'" + object + "%" + "'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int orderid = result.getInt("order_id");
                int cusId = result.getInt("cus_id");
                int empId = result.getInt("emp_id");
                double total = result.getDouble("order_total");
                String cusName = result.getString("cus_Name");
                String empName = result.getString("emp_Name");
                Order order = new Order(orderid, cusId, empId, total);
                order.setCusName(cusName);
                order.setEmpName(empName);
                list.add(order);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getName" + ex.getMessage());
        }
        db.close();
        return list;
    }
    
     public ArrayList<OrderDetail> getDetailAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT orderD_Id ,  order_Id ,  product_Id, orderD_Amount, orderD_Price FROM order_Detail";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
               int orderDId = result.getInt("orderD_Id");
               int orderId = result.getInt("order_Id");
               int productId = result.getInt("product_Id");
               int OrderDAmount = result.getInt("orderD_Amount");
               double orderDPrice = result.getDouble("orderD_Price");
                OrderDetail orderDetail = new OrderDetail(orderDId, orderId, productId, OrderDAmount,orderDPrice);
                list.add(orderDetail);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all order" + ex.getMessage());
        }
        db.close();
        return list;
    }
     
//     public OrderDetail getOrderDetail(int id) {
//        ArrayList list = new ArrayList();
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        //process here
//        try {
//        String sqlDetail = "SELECT od.order_Id as od_id, p.product_name as product_name ,od.orderD_Amount as orderD_Amount , od.orderD_Price as orderD_Price FROM order_detail od JOIN product p WHERE order_Id = ? AND od.product_Id = p.product_id";
//        PreparedStatement stmtDetaill = conn.prepareStatement(sqlDetail);
//        stmtDetaill.setInt(1, id);
//        ResultSet resultDetaill = stmtDetaill.executeQuery();
//        while (resultDetaill.next()) {
//            int orderDId = resultDetaill.getInt("od_id");
//            String product_name = resultDetaill.getString("product_name");
//            int orderD_Amount = resultDetaill.getInt("orderD_Amount");
//            double orderD_Price = resultDetaill.getDouble("orderD_Price");
//            OrderDetail o = new OrderDetail(orderDId, product_name, orderD_Amount, orderD_Price);
//            System.out.println(o.getOrder_Id()+" : "+o.getProductName());
//            return o;
//        }
//         } catch (SQLException ex) {
//            System.out.println("Error: Unable to select receipt id ++++" + id +" "+ ex.getMessage());
//        }
//        db.close();
//        return null;
//    }

        public int addDetail(OrderDetail object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO order_Detail ( order_Id,product_Id,orderD_Amount,orderD_Price ) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getOrder_Id());
            stmt.setInt(2, object.getProduct_Id());
            stmt.setInt(3, object.getOrderD_Amount());
            stmt.setDouble(4, object.getOrderD_Price());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: fail to add");
        }
        db.close();
        return id;
    }
}
