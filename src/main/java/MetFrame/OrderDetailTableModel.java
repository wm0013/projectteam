/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MetFrame;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.OrderDetail;

/**
 *
 * @author user
 */
public class OrderDetailTableModel extends AbstractTableModel {

    private ArrayList<OrderDetail> orderDetail;
    private String[] col = {"Order ID", "Product_Name", "Amount", "Total"};

    public OrderDetailTableModel(ArrayList<OrderDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }

    @Override
    public int getRowCount() {
        return this.orderDetail.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        OrderDetail orderDetails = this.orderDetail.get(rowIndex);
        if (columnIndex == 0) {
            return orderDetails.getOrder_Id();
        }

        if (columnIndex == 1) {
            return orderDetails.getProductName();
        }

        if (columnIndex == 2) {
            return orderDetails.getOrderD_Amount();
        }
        if (columnIndex == 3) {
            return orderDetails.getOrderD_Price();
        }

        return "";
    }

    @Override
    public String getColumnName(int column) {
        return col[column];
    }

}
